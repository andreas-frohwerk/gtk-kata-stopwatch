#include <stdbool.h>

#include <math.h>
#include <gtk/gtk.h>

typedef struct _AppState {
	gboolean  active;
	gint64 start;
	gint64 offset;
	GtkLabel* label;
} AppState;

static guint workshop_text_length(gint64 time_diff_ms) {
	return 9 + (time_diff_ms < 600000 ? 0 : (int) log10((double) time_diff_ms / 10000.0));
}

static gboolean workshop_stopwatch_timeout(AppState* state) {
	if (!(state->active)) return false;

	gint64 now = g_get_monotonic_time();
	gint64 diff = (now - state->start) / 1000;

	gint64 ms = diff % 1000;
	gint64 remaining = (diff - ms) / 1000;
	gint64 sec = remaining % 60;
	gint64 min = remaining / 60;

	guint num_chars = workshop_text_length(diff);

	char text[num_chars];
	snprintf(text, num_chars, "%ld:%02ld.%03ld", min, sec, ms);
	gtk_label_set_text(state->label, text);

	return true;
}

static void workshop_start_button_clicked(GtkWidget *widget, AppState* state) {
	if (!state->active) {
		state->start = g_get_monotonic_time() - state->offset;
		state->active = true;
		g_timeout_add(21, G_SOURCE_FUNC(workshop_stopwatch_timeout), state);
	}
}

static void workshop_stop_button_clicked(GtkWidget *widget, AppState* state) {
	if (state->active) {
		state->offset = g_get_monotonic_time() - state->start;
		state->active = false;
	}
}

static void workshop_reset_button_clicked(GtkWidget *widget, AppState* state) {
	gtk_label_set_text(GTK_LABEL(state->label), "0:00.000");
	state->start = state->active ? g_get_monotonic_time() : 0;
	state->offset = 0;
}

static void workshop_application_activate(GtkApplication *application, AppState* state) {
	GtkWidget *window = gtk_application_window_new(application);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Example: Stopwatch");
	gtk_window_set_default_size(GTK_WINDOW(window), 200, 200);
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);

	GtkWidget *root_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(window), root_box);

	GtkWidget *label = gtk_label_new("0:00.000");
	gtk_box_set_center_widget(GTK_BOX(root_box), label);

	PangoAttrList *labelAttributes = pango_attr_list_new();
	pango_attr_list_change(labelAttributes, pango_attr_size_new_absolute(42 * PANGO_SCALE));
	gtk_label_set_attributes(GTK_LABEL(label), labelAttributes);
	pango_attr_list_unref(labelAttributes);

	GtkWidget *button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_container_set_border_width(GTK_CONTAINER(button_box), 10);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(button_box), GTK_BUTTONBOX_CENTER);
	gtk_box_set_spacing(GTK_BOX(button_box), 10);
	gtk_container_add(GTK_CONTAINER(root_box), button_box);

	GtkWidget *start_button = gtk_button_new_with_label("Start");
	g_signal_connect(start_button, "clicked", G_CALLBACK(workshop_start_button_clicked), state);
	gtk_container_add(GTK_CONTAINER(button_box), start_button);

	GtkWidget *stop_button = gtk_button_new_with_label("Stop");
	g_signal_connect(stop_button, "clicked", G_CALLBACK(workshop_stop_button_clicked), state);
	gtk_container_add(GTK_CONTAINER(button_box), stop_button);

	GtkWidget *reset_button = gtk_button_new_with_label("Reset");
	g_signal_connect(reset_button, "clicked", G_CALLBACK(workshop_reset_button_clicked), state);
	gtk_container_add(GTK_CONTAINER(button_box), reset_button);

	state->label = GTK_LABEL(label);

	gtk_widget_show_all(window);
}

int main(int argc, char **argv) {
	AppState *state = g_new(AppState, 1);
	state->active = false;
	state->start = 0;
	state->offset = 0;

	GtkApplication *app = gtk_application_new("de.frohwerk.workshop", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(workshop_application_activate), state);

	int status = g_application_run(G_APPLICATION(app), argc, argv);

	g_free(state);
	g_object_unref(app);
	return status;
}
